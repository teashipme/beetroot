var productBox = document.getElementById('productBox'),
    itemBox = document.getElementsByClassName('product-box__item'),
    itemBtn = document.getElementsByClassName('product-box__btn'),
    itemBoxQty = document.getElementsByClassName('qty__item'),

    cartQty = document.getElementById('cartQty'),
    cartPrice = document.getElementById('cartPrice');

    productBox.addEventListener('click', (e)=>{
            for(var i=0;i<itemBox.length;i++){
                if(e.target == itemBtn[i]){
                    var qtySum = Number(cartQty.getAttribute('data-price')) + Number(itemBoxQty[i].value);
                    cartQty.setAttribute('data-price', qtySum)
                    cartQty.innerHTML = qtySum;

                    var priceSum = Number(cartPrice.getAttribute('data-price')) + Number(itemBoxQty[i].value) * Number(itemBox[i].getAttribute('data-price'));
                    cartPrice.setAttribute('data-price', priceSum)
                    cartPrice.innerHTML = priceSum
                }
            }
        })

var selectCategory = document.querySelector('.select-box > .select-control');
        
selectCategory.addEventListener('change', ()=>{
    for(var i=0;i<itemBox.length;i++){
        var itemCategoryData = Number(itemBox[i].getAttribute('data-category'));
        itemBox[i].classList.add('hidden');
        if(itemCategoryData == selectCategory.value){
            
            itemBox[i].classList.remove('hidden');
        }
        else if (selectCategory.value == 0) {
            itemBox[i].classList.remove('hidden');
        }
    }
});

var selectPrice = document.querySelector('.price-select-box > .select-control');
        
selectPrice.addEventListener('change', ()=>{
    for(var i=0;i<itemBox.length;i++){
        var itemPriceData = Number(itemBox[i].getAttribute('data-price'));
        itemBox[i].classList.add('hidden');
        if(itemPriceData <= selectPrice.value){
            
            itemBox[i].classList.remove('hidden');
        }
    }
});
 

var buyBtn = document.getElementById('buyBtn'),
modalWrap = document.getElementById('modalWrap'),
modalForm = document.getElementById('modalForm');

buyBtn.addEventListener('click', ()=>{
    modalWrap.classList.remove('hidden');
})
modalForm.addEventListener('submit', ()=>{
    modalForm.classList.add('hidden');
    alert('Спасибо, ваш заказ принят');
})